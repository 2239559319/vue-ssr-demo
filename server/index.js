const Koa = require('koa');
const { compile } = require('handlebars');
const { readFile } = require('fs/promises');
const { join } = require('path');

const ssrApp = require('../dist/server/main.js').default;
const { renderToString } = require('xiaochuan-vue3-source/dist/3.3.4/server-renderer.cjs.prod.js');


const app = new Koa();
const port = 9000;

const clientPath = join(__dirname, '../dist/client');

app.use(async(ctx) => {
  switch (ctx.url) {
    case '/':
    case '/index.html': {
      const template = compile(await readFile(join(__dirname, 'index.hbs'), 'utf-8'));

      const html = await renderToString(ssrApp);
      const res = template({ content: html });

      ctx.body = res;
      ctx.type = 'html';

      break;
    }
    case '/main.css': {
      const css = await readFile(join(clientPath, 'main.css'), 'utf-8');
      ctx.body = css;
      ctx.type = 'css';
      break;
    }
    case '/main.js': {
      const js = await readFile(join(clientPath, 'main.js'), 'utf-8');
      ctx.body = js;
      ctx.type = 'js';
      break;
    }
    case '/main.js.map': {
      const js = await readFile(join(clientPath, 'main.js.map'), 'utf-8');
      ctx.body = js;
      break;
    }
    default:
      break;
  }
});

app.listen(port, () => {
  console.log(`server in port ${port}`);
});
