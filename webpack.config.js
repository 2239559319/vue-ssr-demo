const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const { VueLoaderPlugin } = require('vue-loader');
const { join } = require('path');

/**
 * @type {import('webpack').Configuration}
 */
const configClient = {
  entry: './src/index.ts',
  output: {
    filename: 'main.js',
    path: join(__dirname, 'dist', 'client')
  },

  devtool: 'source-map',
  mode: 'production',
  optimization: {
    minimize: false
  },
  resolve: {
    extensions: ['.ts', '.vue', '.js']
  },

  module: {
    rules: [
      {
        test: /\.vue$/,
        use: 'vue-loader'
      },
      {
        test: /\.css$/,
        use: [
          MiniCssExtractPlugin.loader,
          'css-loader',
        ]
      },
      {
        test: /\.tsx?$/,
        loader: 'ts-loader',
        options: {
          transpileOnly: true,
          appendTsSuffixTo: ['\\.vue$'],  
        }
      }
    ]
  },
  externals: {
    vue: 'Vue'
  },
  plugins: [
    new MiniCssExtractPlugin(),
    new VueLoaderPlugin()
  ]
};

/**
 * @type {import('webpack').Configuration}
 */
const configServer = {
  entry: './src/main.ts',
  output: {
    filename: 'main.js',
    path: join(__dirname, 'dist', 'server'),
    library: {
      type: 'commonjs'
    }
  },
  target: 'node',
  devtool: 'source-map',
  mode: 'production',
  optimization: {
    minimize: false
  },
  resolve: {
    extensions: ['.ts', '.vue', '.js'],
    // alias: {
    //   '@vue/server-renderer': 'xiaochuan-vue3-source/dist/3.3.4/server-renderer.cjs.prod.js'
    // }
  },

  module: {
    rules: [
      {
        test: /\.vue$/,
        use: 'vue-loader'
      },
      {
        test: /\.css$/,
        use: [
          MiniCssExtractPlugin.loader,
          'css-loader',
        ]
      },
      {
        test: /\.tsx?$/,
        loader: 'ts-loader',
        options: {
          transpileOnly: true,
          appendTsSuffixTo: ['\\.vue$'],  
        }
      }
    ]
  },
  externals: [
    {
      vue: 'vue',
      '@vue/server-renderer': 'xiaochuan-vue3-source/dist/3.3.4/server-renderer.cjs.prod.js'
    },
  ],
  plugins: [
    new MiniCssExtractPlugin(),
    new VueLoaderPlugin()
  ]
};


module.exports = [configClient, configServer];
